package com.example.jeremy.dictionary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jeremy.dictionary.dictionarydto.Dictionary;
import com.example.jeremy.dictionary.worddefinition.Definition;
import com.example.jeremy.dictionary.worddefinition.WordDefinition;
import com.google.gson.Gson;

import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
{
    private EditText searchText;
    private ListView definitionListView;
    private Button btnLookup;

    private WordDefinition wordDefinition;
    private ArrayList<Dictionary> selectedDictionaries;

    private boolean overrideSelectedDictionariesOnResume;

    private static final int SELECT_DICTIONARIES_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchText = (EditText) findViewById(R.id.wordEditTextView);
        definitionListView = (ListView) findViewById(R.id.definitionListView);
        btnLookup = (Button) findViewById(R.id.lookupButton);

        // move this to on resume ?
        wordDefinition = null;
        selectedDictionaries = null;
        overrideSelectedDictionariesOnResume = false;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreference.edit();

        Gson gson = new Gson();
        if (wordDefinition != null)
            editor.putString("wordDefinition", gson.toJson(wordDefinition));
        if (selectedDictionaries != null)
            editor.putString("selectedDictionaries", gson.toJson(selectedDictionaries.toArray()));

        editor.commit();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);

        Gson gson = new Gson();
        String wordDefinitionToJson = sharedPreference.getString("wordDefinition", "");
        String selectedDictionariesJson = sharedPreference.getString("selectedDictionaries", "");

        if (wordDefinitionToJson.equals(""))
        {
            wordDefinition = null;
            return;
        }

        wordDefinition = gson.fromJson(wordDefinitionToJson, WordDefinition.class);

        if (overrideSelectedDictionariesOnResume)
        {
            if (!selectedDictionariesJson.equals(""))
            {
                Dictionary[] dictionaries = gson.fromJson(selectedDictionariesJson, Dictionary[].class);

                selectedDictionaries = new ArrayList<>(dictionaries.length);
                for (Dictionary dic : dictionaries)
                    selectedDictionaries.add(dic);
            }
            else
                selectedDictionaries = null;
        }

        overrideSelectedDictionariesOnResume = true;

        searchText.setText(wordDefinition.getWord());

        fillList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lookup_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case R.id.selectDictionariesMenuItem:
                Intent i = new Intent(this, DictionarySelectionActivity.class);
                if (selectedDictionaries != null)
                    i.putParcelableArrayListExtra("selectedDictionaries", selectedDictionaries);
                startActivityForResult(i, SELECT_DICTIONARIES_REQUEST_CODE);
                return true;
            case R.id.shareMenuItem:
                if (wordDefinition == null)
                    return super.onOptionsItemSelected(item);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "some@email.address" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Definitions of word: " + wordDefinition.getWord());
                intent.putExtra(Intent.EXTRA_TEXT, wordDefinition.getDefinitionsFromShownDictionaries());

                startActivity(Intent.createChooser(intent, "Send mail..."));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK && requestCode == SELECT_DICTIONARIES_REQUEST_CODE)
        {
            if (data.hasExtra("selectedDictionaries"))
            {
                selectedDictionaries = data.getExtras().getParcelableArrayList("selectedDictionaries");
                overrideSelectedDictionariesOnResume = false;
                fillList();
            }
        }
    }

    public void lookUpButtonClicked(View v)
    {
        try
        {
            btnLookup.setEnabled(false);
            wordDefinition = new LoadDefinitionsWebServiceTask().execute(searchText.getText().toString()).get();
            fillList();
        } catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), R.string.error_word_definition, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        finally
        {
            btnLookup.setEnabled(true);
        }
    }

    private void fillList()
    {
        if (wordDefinition == null)
        {
            definitionListView.setAdapter(new ArrayAdapter<Definition>(this, android.R.layout.simple_list_item_1, new ArrayList<Definition>()));
            return;
        }

        wordDefinition.filterDefinitions(selectedDictionaries);
        ArrayAdapter<Definition> definitions = new ArrayAdapter<Definition>(this, android.R.layout.simple_list_item_1, wordDefinition.getDefinitionsToShow());
        definitionListView.setAdapter(definitions);
    }

    private class LoadDefinitionsWebServiceTask extends AsyncTask<String, Void, WordDefinition>
    {

        @Override
        protected WordDefinition doInBackground(String... params)
        {
            try
            {
                final String url = "http://services.aonaware.com/DictService/DictService.asmx/Define?word=" + params[0];
                RestTemplate template = new RestTemplate();
                SimpleXmlHttpMessageConverter xmlConv = new SimpleXmlHttpMessageConverter();
                template.getMessageConverters().add(xmlConv);
                WordDefinition wordDefinitions = template.getForObject(url, WordDefinition.class);
                return wordDefinitions;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }
    }
}
