package com.example.jeremy.dictionary.worddefinition;

import com.example.jeremy.dictionary.dictionarydto.Dictionary;

import org.simpleframework.xml.Element;

/**
 * Created by Jeremy on 19/10/2016.
 */
public class Definition
{
    @Element(name="Word")
    private String word;
    @Element(name="Dictionary")
    private Dictionary dictionary;
    @Element(name="WordDefinition")
    private String wordDefinition;

    public String getWord()
    {
        return word;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public Dictionary getDictionary()
    {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary)
    {
        this.dictionary = dictionary;
    }

    public String getWordDefinition()
    {
        return wordDefinition;
    }

    public void setWordDefinition(String wordDefinition)
    {
        this.wordDefinition = wordDefinition;
    }

    @Override
    public String toString()
    {
        return wordDefinition;
    }
}
