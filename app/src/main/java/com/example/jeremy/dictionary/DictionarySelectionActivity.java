package com.example.jeremy.dictionary;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.jeremy.dictionary.dictionarydto.ArrayOfDictionaries;
import com.example.jeremy.dictionary.dictionarydto.Dictionary;
import com.example.jeremy.dictionary.worddefinition.WordDefinition;

import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Jeremy on 17/10/2016.
 */

public class DictionarySelectionActivity extends Activity
{
    private ListView dictionariesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_selection);
        dictionariesListView = (ListView) findViewById(R.id.dictionariesListView);

        try
        {
            ArrayOfDictionaries dictionaries = new LoadDictionariesWebServiceTask().execute().get();
            ArrayAdapter<Dictionary> adapter = new ArrayAdapter<Dictionary>(this, android.R.layout.simple_list_item_multiple_choice, dictionaries.getDictionaries());
            dictionariesListView.setAdapter(adapter);

            Bundle extras = getIntent().getExtras();
            if (extras != null)
            {
                ArrayList<Dictionary> selectedDictionaries = extras.getParcelableArrayList("selectedDictionaries");
                for (int i = 0; i < dictionaries.getDictionaries().size(); i++)
                    if (selectedDictionaries.contains(adapter.getItem(i)))
                        dictionariesListView.setItemChecked(i, true);
            }
            else
                for (int i = 0; i < dictionaries.getDictionaries().size(); i++)
                    dictionariesListView.setItemChecked(i, true);

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void finish()
    {
        // Prepare data intent
        Intent data = new Intent();
        // Get Selected Dictionaries...
        int len = dictionariesListView.getCount();
        SparseBooleanArray checked = dictionariesListView.getCheckedItemPositions();
        ArrayList<Dictionary> selectedDictionaries = new ArrayList<>(checked.size());

        for (int i = 0; i < len; i++)
            if (checked.get(i))
                selectedDictionaries.add((Dictionary) dictionariesListView.getAdapter().getItem(i));

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("selectedDictionaries", selectedDictionaries);
        data.putExtras(bundle);
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        super.finish();
    }

    private class LoadDictionariesWebServiceTask extends AsyncTask<String, Void, ArrayOfDictionaries>
    {

        @Override
        protected ArrayOfDictionaries doInBackground(String... params)
        {
            try
            {
                final String url = "http://services.aonaware.com//DictService/DictService.asmx/DictionaryList?";
                RestTemplate template = new RestTemplate();
                SimpleXmlHttpMessageConverter xmlConv = new SimpleXmlHttpMessageConverter();
                template.getMessageConverters().add(xmlConv);
                ArrayOfDictionaries dictionaries = template.getForObject(url, ArrayOfDictionaries.class);
                return dictionaries;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }
    }
}

