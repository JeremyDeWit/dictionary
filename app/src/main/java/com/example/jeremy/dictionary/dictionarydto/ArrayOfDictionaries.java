package com.example.jeremy.dictionary.dictionarydto;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by jeremy on 21/10/2016.
 */

public class ArrayOfDictionaries
{
    @ElementList(entry="Dictionary", inline=true, required=false)
    private List<Dictionary> dictionaries;

    public ArrayOfDictionaries() { }

    public List<Dictionary> getDictionaries()
    {
        return dictionaries;
    }

    public void setDictionaries(List<Dictionary> dictionaries)
    {
        this.dictionaries = dictionaries;
    }
}
