package com.example.jeremy.dictionary.worddefinition;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by Jeremy on 19/10/2016.
 */

public class DefinitionsList
{
    @ElementList(entry="Definition", inline=true, required=false)
    private List<Definition> definitions;

    public DefinitionsList()
    {

    }

    public List<Definition> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(List<Definition> definitions) {
        this.definitions = definitions;
    }
}
