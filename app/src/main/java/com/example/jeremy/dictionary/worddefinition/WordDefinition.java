package com.example.jeremy.dictionary.worddefinition;

import com.example.jeremy.dictionary.dictionarydto.Dictionary;

import org.simpleframework.xml.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jeremy on 19/10/2016.
 */

public class WordDefinition
{
    @Element(name="Word")
    private String word;

    @Element(name="Definitions")
    private DefinitionsList definitions;

    private List<Definition> definitionsToShow;

    public WordDefinition() { }

    public String getWord()
    {
        return word;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public List<Definition> getDefinitions()
    {
        return definitions.getDefinitions();
    }

    public void setDefinitions(List<Definition> definitions)
    {
        this.definitions.setDefinitions(definitions);
    }

    public List<Definition> getDefinitionsToShow()
    {
        if (definitionsToShow == null)
            resetDefinitionsToShow();

        return definitionsToShow;
    }

    public void setDefinitionsToShow(List<Definition> definitionsToShow)
    {
        this.definitionsToShow = definitionsToShow;
    }

    public void resetDefinitionsToShow()
    {
        if (definitions == null || definitions.getDefinitions() == null || definitions.getDefinitions().isEmpty())
        {
            definitionsToShow = new ArrayList<>();
            return;
        }

        ArrayList<Definition> defs = new ArrayList<>(getDefinitions().size());
        for (Definition def : getDefinitions())
            defs.add(def);

        definitionsToShow = defs;
    }

    public String getDefinitionsFromShownDictionaries()
    {
        String text = "";
        List<Definition> defs = getDefinitionsToShow();
        for (Definition def : defs)
        {
            text += def.getDictionary().getName() + "\n";
            text += def.getWordDefinition() + "\n";
        }

        return text;
    }

    public void filterDefinitions(ArrayList<Dictionary> dictionariesIWant)
    {
        if (dictionariesIWant == null)
            return;

        resetDefinitionsToShow();
        Iterator<Definition> itr = getDefinitionsToShow().iterator();

        while (itr.hasNext())
        {
            Definition currentDefinition = itr.next();
            if (!dictionariesIWant.contains(currentDefinition.getDictionary()))
                itr.remove();
        }
    }
}
